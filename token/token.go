package token

import "strconv"

// TokenType represents the type of token.
type TokenType string

const (
	// Special tokens
	ILLEGAL = "ILLEGAL"
	EOF     = "EOF"
	WS      = "WS" // White space

	// Literals
	IDENT  = "IDENT"  // foo
	NUMBER = "NUMBER" // 123
	STRING = "STRING" // "hello world"

	// Operators
	IGUAL        = "="
	MAIS         = "+"
	MENOS        = "-"
	ASTERISCO    = "*"
	BARRA        = "/"
	MODULO       = "%"
	MENORQUE     = "<"
	MAIORQUE     = ">"
	MENORIGUAL   = "<="
	MAIORIGUAL   = ">="
	DIFERENTE    = "!="
	IGUALDADE    = "=="
	ATRIBUICAO   = ":="
	ELOGICO      = "&&"
	OULOGICO     = "||"
	NEGACAO      = "!"
	BIT_E        = "&"
	BIT_OU       = "|"
	BIT_XOR      = "^"
	BIT_CLEAR    = "&^"
	SHIFT_ESQ    = "<<"
	SHIFT_DIR    = ">>"
	INCREMENTO   = "++"
	DECREMENTO   = "--"
	PONTO        = "."
	VIRGULA      = ","
	PONTOVIRGULA = ";"

	// Delimiters
	ABREPAREN  = "("
	FECHAPAREN = ")"
	ABRECOLCH  = "["
	FECHACOLCH = "]"
	ABRECHAVE  = "{"
	FECHACHAVE = "}"

	// Keywords
	E        = "e"
	OU       = "ou"
	SE       = "se"
	SENAO    = "senão"
	PARA     = "para"
	ENQUANTO = "enquanto"
	RETORNE  = "retorne"
	VAR      = "var"
	LEIA     = "leia"
	ESCREVA  = "escreva"

	COMENTARIO = "//"
	DOISPONTOS = ":"

	VERDADEIRO = "verdadeiro"
	FALSO      = "falso"
)

// Token represents a token in the Pog programming language.
type Token struct {
	Type    TokenType
	Literal string
	Line    int
	Char    int
}

// Keywords is a map that contains the keyword tokens for the Pog programming language.
var Keywords = map[string]TokenType{
	E:        ELOGICO,
	OU:       OULOGICO,
	SE:       SE,
	SENAO:    SENAO,
	PARA:     PARA,
	ENQUANTO: ENQUANTO,
	RETORNE:  RETORNE,
	VAR:      VAR,
	LEIA:     LEIA,
	ESCREVA:  ESCREVA,
}

// LookupIdent returns the token type for a given identifier.
func LookupIdent(ident string) TokenType {
	if tok, ok := Keywords[ident]; ok {
		return tok
	}
	return IDENT
}

// LookupNumber returns the token type for a given number literal.
func LookupNumber(number string) TokenType {
	// try to parse the number
	_, err := strconv.ParseInt(number, 10, 64)
	if err == nil {
		return NUMBER
	}

	_, err = strconv.ParseFloat(number, 64)
	if err == nil {
		return NUMBER
	}

	return ILLEGAL
}

// LookupString returns the token type for a given string literal.
func LookupString(str string) TokenType {
	_, err := strconv.Unquote(str)
	if err == nil {
		return STRING
	}
	return ILLEGAL
}
