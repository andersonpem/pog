package lexer

import (
	"pog/token"
	"strings"
)

// Lexer represents a lexer for the Pog programming language.
type Lexer struct {
	input        string
	position     int  // current position in input (points to current char)
	readPosition int  // current reading position in input (after current char)
	line         int  // current line number
	char         int  // current char number in line
	ch           rune // current char under examination
}

// isWhitespace returns true if the character is a whitespace character.
func isWhitespace(ch rune) bool {
	return ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r'
}

// isComment returns true if the character is the beginning of a comment.
func isComment(ch rune) bool {
	return ch == '/'
}

// isDigit returns true if the character is a digit.
func isDigit(ch rune) bool {
	return '0' <= ch && ch <= '9'
}

// New returns a new instance of Lexer.
func New(input string) *Lexer {
	l := &Lexer{input: input, line: 1}
	l.readChar()
	return l
}

// isLetter returns true if the character is a letter or underscore.
func isLetter(ch rune) bool {
	return 'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z' || ch == '_'
}

// NextToken returns the next token from the input.
func (l *Lexer) NextToken() token.Token {
	var tok token.Token

	// skip whitespace and comments
	for isWhitespace(l.ch) || isComment(l.ch) {
		if isWhitespace(l.ch) {
			l.skipWhitespace()
		} else {
			tok = l.scanComment()
			if tok.Type != token.WS {
				return tok
			}
		}
	}

	// check for special characters
	switch l.ch {
	case '=':
		if l.peekChar() == '=' {
			ch := l.ch
			l.readChar()
			literal := string(ch) + string(l.ch)
			tok = token.Token{Type: token.IGUALDADE, Literal: literal, Line: l.line, Char: l.char}
		} else {
			tok = newToken(token.IGUAL, l.ch, l.line, l.char)
		}
	case '+':
		tok = newToken(token.MAIS, l.ch, l.line, l.char)
	case '-':
		tok = newToken(token.MENOS, l.ch, l.line, l.char)
	case '*':
		tok = newToken(token.ASTERISCO, l.ch, l.line, l.char)
	case '/':
		tok = newToken(token.BARRA, l.ch, l.line, l.char)
	case '%':
		tok = newToken(token.MODULO, l.ch, l.line, l.char)
	case '<':
		if l.peekChar() == '=' {
			ch := l.ch
			l.readChar()
			literal := string(ch) + string(l.ch)
			tok = token.Token{Type: token.MENORIGUAL, Literal: literal, Line: l.line, Char: l.char}
		} else {
			tok = newToken(token.MENORQUE, l.ch, l.line, l.char)
		}
	case '>':
		if l.peekChar() == '=' {
			ch := l.ch
			l.readChar()
			literal := string(ch) + string(l.ch)
			tok = token.Token{Type: token.MAIORIGUAL, Literal: literal, Line: l.line, Char: l.char}
		} else {
			tok = newToken(token.MAIORQUE, l.ch, l.line, l.char)
		}
	case '!':
		if l.peekChar() == '=' {
			ch := l.ch
			l.readChar()
			literal := string(ch) + string(l.ch)
			tok = token.Token{Type: token.DIFERENTE, Literal: literal, Line: l.line, Char: l.char}
		} else {
			tok = newToken(token.NEGACAO, l.ch, l.line, l.char)
		}
	case ':':
		if l.peekChar() == '=' {
			ch := l.ch
			l.readChar()
			literal := string(ch) + string(l.ch)
			tok = token.Token{Type: token.ATRIBUICAO, Literal: literal, Line: l.line, Char: l.char}
		} else {
			tok = newToken(token.PONTOVIRGULA, l.ch, l.line, l.char)
		}
	case '&':
		if l.peekChar() == '&' {
			ch := l.ch
			l.readChar()
			literal := string(ch) + string(l.ch)
			tok = token.Token{Type: token.ELOGICO, Literal: literal, Line: l.line, Char: l.char}
		} else if l.peekChar() == '^' {
			ch := l.ch
			l.readChar()
			literal := string(ch) + string(l.ch)
			tok = token.Token{Type: token.BIT_CLEAR, Literal: literal, Line: l.line, Char: l.char}
		} else {
			tok = newToken(token.BIT_E, l.ch, l.line, l.char)
		}
	case '|':
		if l.peekChar() == '|' {
			ch := l.ch
			l.readChar()
			literal := string(ch) + string(l.ch)
			tok = token.Token{Type: token.OULOGICO, Literal: literal, Line: l.line, Char: l.char}
		} else {
			tok = newToken(token.BIT_OU, l.ch, l.line, l.char)
		}
	case '^':
		tok = newToken(token.BIT_XOR, l.ch, l.line, l.char)
	case '(':
		tok = newToken(token.ABREPAREN, l.ch, l.line, l.char)
	case ')':
		tok = newToken(token.FECHAPAREN, l.ch, l.line, l.char)
	case '[':
		tok = newToken(token.ABRECOLCH, l.ch, l.line, l.char)
	case ']':
		tok = newToken(token.FECHACOLCH, l.ch, l.line, l.char)
	case '{':
		tok = newToken(token.ABRECHAVE, l.ch, l.line, l.char)
	case '}':
		tok = newToken(token.FECHACHAVE, l.ch, l.line, l.char)
	case ';':
		tok = newToken(token.PONTOVIRGULA, l.ch, l.line, l.char)
	case ',':
		tok = newToken(token.VIRGULA, l.ch, l.line, l.char)
	case '.':
		if isDigit(l.peekChar()) {
			return l.scanNumber()
		}
		tok = newToken(token.PONTO, l.ch, l.line, l.char)
	case '"':
		return l.scanString()
	case 0:
		return token.Token{Type: token.EOF, Literal: ""}
	default:
		return token.Token{Type: token.ILLEGAL, Literal: string(l.ch)}
	}

	l.readChar()
	return tok
}

// newToken returns a new token with the given type, literal, line and character number
func newToken(tokenType token.TokenType, ch rune, line, char int) token.Token {
	return token.Token{Type: tokenType, Literal: string(ch), Line: line, Char: char}
}

// readChar reads the next character in the input and advances the lexer's position
func (l *Lexer) readChar() {
	if l.readPosition >= len(l.input) {
		l.ch = 0 // ASCII code for NUL
	} else {
		l.ch = rune(l.input[l.readPosition])
	}

	if l.ch == '\n' {
		l.line++
		l.char = 0
	} else {
		l.char++
	}

	l.position = l.readPosition
	l.readPosition++
}

// peekChar returns the next character in the input without advancing the lexer's position
func (l *Lexer) peekChar() rune {
	if l.readPosition >= len(l.input) {
		return 0
	}
	return rune(l.input[l.readPosition])
}

// skipWhitespace skips any whitespace characters in the input.
func (l *Lexer) skipWhitespace() {
	for isWhitespace(l.ch) {
		l.readChar()
	}
}

// scanComment scans a comment in the input.
func (l *Lexer) scanComment() token.Token {
	var tok token.Token

	// expect a double slash
	if l.ch != '/' || l.peekChar() != '/' {
		return newToken(token.ILLEGAL, l.ch, l.line, l.char)
	}

	// consume the double slash and read until the end of the line
	l.readChar()
	l.readChar()

	var commentValue strings.Builder
	for l.ch != '\n' && l.ch != 0 {
		commentValue.WriteRune(l.ch)
		l.readChar()
	}

	tok.Type = token.COMENTARIO
	tok.Literal = commentValue.String()
	tok.Line = l.line
	tok.Char = l.char - len(commentValue.String()) - 2

	return tok
}

// scanIdentifier scans an identifier and returns a token with the appropriate type.
func (l *Lexer) scanIdentifier() token.Token {
	var ident strings.Builder
	for isLetter(l.ch) || isDigit(l.ch) || l.ch == '_' {
		ident.WriteRune(l.ch)
		l.readChar()
	}

	l.Backup()

	identStr := ident.String()
	if tokType := token.LookupIdent(identStr); tokType != token.IDENT {
		return token.Token{Type: tokType, Literal: identStr}
	}
	return token.Token{Type: token.IDENT, Literal: identStr}
}

// scanNumber scans a number literal in the input.
func (l *Lexer) scanNumber() token.Token {
	var tok token.Token
	tok.Line = l.line
	tok.Char = l.char

	var num strings.Builder
	for isDigit(l.ch) {
		num.WriteRune(l.ch)
		l.readChar()
	}

	if l.ch == '.' {
		num.WriteRune(l.ch)
		l.readChar()

		for isDigit(l.ch) {
			num.WriteRune(l.ch)
			l.readChar()
		}

		tok.Type = token.NUMBER
		tok.Literal = num.String()
		return tok
	}

	tok.Type = token.LookupNumber(num.String())
	tok.Literal = num.String()
	return tok
}

// scanString scans a string literal in the input.
func (l *Lexer) scanString() token.Token {
	var tok token.Token
	tok.Type = token.STRING
	tok.Line = l.line
	tok.Char = l.char

	l.readChar() // consume the opening double quote

	var str strings.Builder
	for l.ch != '"' && l.ch != 0 {
		if l.ch == '\\' && l.peekChar() == 'n' {
			l.readChar()
			l.readChar()
			str.WriteRune('\n')
		}
		if l.peekChar() == 'n' {
			l.readChar()
			str.WriteRune('\n')
		} else if l.peekChar() == 't' {
			l.readChar()
			str.WriteRune('\t')
		} else if l.peekChar() == '"' {
			l.readChar()
			str.WriteRune('"')
		} else if l.peekChar() == '\\' {
			l.readChar()
			str.WriteRune('\\')
		} else {
			str.WriteRune(l.ch)
		}
		l.readChar()
	}
	l.readChar() // consume the closing double quote

	tok.Literal = str.String()

	return tok
}

// nextToken returns the next token in the input.
func (l *Lexer) nextToken() token.Token {
	l.skipWhitespace()

	if isComment(l.ch) {
		return l.scanComment()
	}

	if isDigit(l.ch) {
		return l.scanNumber()
	}

	if isLetter(l.ch) {
		return l.scanIdentifier()
	}

	switch l.ch {
	case '"':
		return l.scanString()
	case ':':
		if l.peekChar() == '=' {
			l.readChar()
			return token.Token{Type: token.ATRIBUICAO, Literal: ":="}
		} else {
			return token.Token{Type: token.DOISPONTOS, Literal: ":"}
		}
	case '+':
		return token.Token{Type: token.MAIS, Literal: string(l.ch)}
	case '-':
		return token.Token{Type: token.MENOS, Literal: string(l.ch)}
	case '*':
		return token.Token{Type: token.ASTERISCO, Literal: string(l.ch)}
	case '/':
		return token.Token{Type: token.BARRA, Literal: string(l.ch)}
	case '%':
		return token.Token{Type: token.MODULO, Literal: string(l.ch)}
	case '(':
		return token.Token{Type: token.ABREPAREN, Literal: string(l.ch)}
	case ')':
		return token.Token{Type: token.FECHAPAREN, Literal: string(l.ch)}
	case ',':
		return token.Token{Type: token.VIRGULA, Literal: string(l.ch)}
	case ';':
		return token.Token{Type: token.PONTOVIRGULA, Literal: string(l.ch)}
	case '>':
		if l.peekChar() == '=' {
			l.readChar()
			return token.Token{Type: token.MAIORIGUAL, Literal: ">="}
		} else {
			return token.Token{Type: token.MAIORQUE, Literal: string(l.ch)}
		}
	case '<':
		if l.peekChar() == '=' {
			l.readChar()
			return token.Token{Type: token.MENORIGUAL, Literal: "<="}
		} else {
			return token.Token{Type: token.MENORQUE, Literal: string(l.ch)}
		}
	case '=':
		if l.peekChar() == '=' {
			l.readChar()
			return token.Token{Type: token.IGUALDADE, Literal: "=="}
		} else {
			return token.Token{Type: token.ATRIBUICAO, Literal: string(l.ch)}
		}
	case '!':
		if l.peekChar() == '=' {
			l.readChar()
			return token.Token{Type: token.DIFERENTE, Literal: "!="}
		} else {
			return token.Token{Type: token.NEGACAO, Literal: string(l.ch)}
		}
	case 0:
		return token.Token{Type: token.EOF, Literal: ""}
	default:
		return token.Token{Type: token.ILLEGAL, Literal: string(l.ch)}
	}
}

// Backup steps back one rune in the input.
func (l *Lexer) Backup() {
	if l.position > 0 {
		l.position--
		l.char--
		l.ch = rune(l.input[l.position])
	} else {
		l.ch = 0
	}
}

// PeekToken returns the next token in the input without advancing the lexer's position.
func (l *Lexer) PeekToken() token.Token {
	l.skipWhitespace()

	tok := l.nextToken()
	l.Backup()

	return tok
}
