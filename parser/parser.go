package parser

import (
	"fmt"
	"pog/ast"
	"pog/lexer"
	"pog/token"
	"strconv"
)

const (
	LOWEST int = iota
	EQUALS
	LESSGREATER
	SUM
	PRODUCT
	PREFIX
	CALL
)

var precedences = map[token.TokenType]int{
	token.IGUALDADE: 1,
	token.DIFERENTE: 1,
	token.MENORQUE:  2,
	token.MAIORQUE:  2,
	token.MAIS:      3,
	token.MENOS:     3,
	token.BARRA:     4,
	token.ASTERISCO: 4,
}

// Parser represents a parser for the interpreter.
type Parser struct {
	l            *lexer.Lexer
	errors       []string
	currentToken token.Token
	peekToken    token.Token
}

// New returns a new Parser.
func New(l *lexer.Lexer) *Parser {
	p := &Parser{l: l, errors: []string{}}

	// Read two tokens, so curToken and peekToken are both set.
	p.nextToken()
	p.nextToken()

	return p
}

// Errors returns the list of errors encountered by the parser.
func (p *Parser) Errors() []string {
	return p.errors
}

// peekPrecedence returns the precedence of the next token.
func (p *Parser) peekPrecedence() int {
	if p, ok := precedences[p.peekToken.Type]; ok {
		return p
	}
	return LOWEST
}

// curPrecedence returns the precedence of the current token.
func (p *Parser) curPrecedence() int {
	if p, ok := precedences[p.currentToken.Type]; ok {
		return p
	}
	return LOWEST
}

// nextToken reads the next token from the lexer and advances the parser's
// curToken and peekToken fields.
func (p *Parser) nextToken() {
	p.currentToken = p.peekToken
	p.peekToken = p.l.NextToken()
}

// expectPeek verifies that the next token is of the expected type. If it is,
// the function advances the parser by reading the next token. Otherwise, it
// adds an error to the parser's error list.
func (p *Parser) expectPeek(expectedType token.TokenType) bool {
	if p.peekToken.Type == expectedType {
		p.nextToken()
		return true
	}
	p.peekError(expectedType)
	return false
}

// peekError adds an error to the parser's error list indicating that the next
// token is not of the expected type.
func (p *Parser) peekError(expectedType token.TokenType) {
	msg := fmt.Sprintf("Expected next token to be %s, got %s instead.",
		expectedType, p.peekToken.Type)
	p.errors = append(p.errors, msg)
}

// parseOutputStatement parses an output statement and returns the corresponding AST node.
func (p *Parser) parseOutputStatement() ast.Statement {
	stmt := &ast.OutputStatement{Token: p.currentToken}

	if !p.expectPeek(token.ABREPAREN) {
		return nil
	}

	p.nextToken()
	expression := p.parseExpression(LOWEST)

	stmt.Expression = expression

	if !p.expectPeek(token.FECHAPAREN) {
		return nil
	}

	if !p.expectPeek(token.PONTOVIRGULA) {
		return nil
	}

	return stmt
}

// parseInputStatement parses an input statement and returns the corresponding AST node.
func (p *Parser) parseInputStatement() ast.Statement {
	stmt := &ast.InputStatement{Token: p.currentToken}

	if !p.expectPeek(token.ABREPAREN) {
		return nil
	}

	if !p.expectPeek(token.IDENT) {
		return nil
	}

	name := &ast.Identifier{Token: p.currentToken, Value: p.currentToken.Literal}

	stmt.Target = name

	if !p.expectPeek(token.FECHAPAREN) {
		return nil
	}

	if !p.expectPeek(token.PONTOVIRGULA) {
		return nil
	}

	return stmt
}

// parseFunctionLiteral parses a function literal and returns the corresponding AST node.
func (p *Parser) parseFunctionLiteral() ast.Expression {
	lit := &ast.FunctionLiteral{Token: p.currentToken}

	if !p.expectPeek(token.ABREPAREN) {
		return nil
	}

	lit.Parameters = p.parseFunctionParameters()

	if !p.expectPeek(token.ABRECHAVE) {
		return nil
	}

	lit.Body = p.parseBlockStatement()

	return lit
}

// parseFunctionParameters parses the parameters of a function and returns the corresponding AST node.
func (p *Parser) parseFunctionParameters() []*ast.Identifier {
	identifiers := []*ast.Identifier{}

	if p.peekTokenIs(token.FECHAPAREN) {
		p.nextToken()
		return identifiers
	}

	p.nextToken()

	ident := &ast.Identifier{Token: p.currentToken, Value: p.currentToken.Literal}
	identifiers = append(identifiers, ident)

	for p.peekTokenIs(token.VIRGULA) {
		p.nextToken()
		p.nextToken()

		ident := &ast.Identifier{Token: p.currentToken, Value: p.currentToken.Literal}
		identifiers = append(identifiers, ident)
	}

	if !p.expectPeek(token.FECHAPAREN) {
		return nil
	}

	return identifiers
}

// parseCallExpression parses a function call and returns the corresponding AST node.
func (p *Parser) parseCallExpression(function ast.Expression) ast.Expression {
	exp := &ast.CallExpression{Token: p.currentToken, Function: function}
	exp.Arguments = p.parseExpressionList(token.FECHAPAREN)
	return exp
}

// parseExpressionList parses a comma-separated list of expressions and returns the corresponding AST nodes.
func (p *Parser) parseExpressionList(end token.TokenType) []ast.Expression {
	args := []ast.Expression{}

	if p.peekTokenIs(end) {
		p.nextToken()
		return args
	}

	p.nextToken()
	args = append(args, p.parseExpression(LOWEST))

	for p.peekTokenIs(token.VIRGULA) {
		p.nextToken()
		p.nextToken()
		args = append(args, p.parseExpression(LOWEST))
	}

	if !p.expectPeek(end) {
		return nil
	}

	return args
}

// parseIdentifier parses an identifier and returns the corresponding AST node.
func (p *Parser) parseIdentifier() ast.Expression {
	return &ast.Identifier{
		Token: p.currentToken,
		Value: p.currentToken.Literal,
	}
}

// parseBooleanLiteral parses a boolean literal and returns the corresponding AST node.
func (p *Parser) parseBooleanLiteral() ast.Expression {
	return &ast.BooleanLiteral{
		Token: p.currentToken,
		Value: p.currentTokenIs(token.VERDADEIRO),
	}
}

// parseIntegerLiteral parses an integer literal and returns the corresponding AST node.
func (p *Parser) parseIntegerLiteral() ast.Expression {
	lit := &ast.IntegerLiteral{Token: p.currentToken}

	value, err := strconv.ParseInt(p.currentToken.Literal, 0, 64)
	if err != nil {
		msg := fmt.Sprintf("could not parse %q as integer", p.currentToken.Literal)
		p.errors = append(p.errors, msg)
		return nil
	}

	lit.Value = value
	return lit
}

// parseStringLiteral parses a string literal and returns the corresponding AST node.
func (p *Parser) parseStringLiteral() ast.Expression {
	return &ast.StringLiteral{
		Token: p.currentToken,
		Value: p.currentToken.Literal,
	}
}

// parseArrayLiteral parses an array literal and returns the corresponding AST node.
func (p *Parser) parseArrayLiteral() ast.Expression {
	array := &ast.ArrayLiteral{Token: p.currentToken}

	array.Elements = p.parseExpressionList(token.FECHACOLCH)

	return array
}

// parseHashLiteral parses a hash literal and returns the corresponding AST node.
func (p *Parser) parseHashLiteral() ast.Expression {
	hash := &ast.HashLiteral{Token: p.currentToken}
	hash.Pairs = make(map[ast.Expression]ast.Expression)

	for !p.peekTokenIs(token.FECHACHAVE) {
		p.nextToken()
		key := p.parseExpression(LOWEST)

		if !p.expectPeek(token.VIRGULA) {
			return nil
		}

		p.nextToken()
		value := p.parseExpression(LOWEST)

		hash.Pairs[key] = value

		if !p.peekTokenIs(token.FECHACHAVE) && !p.expectPeek(token.VIRGULA) {
			return nil
		}
	}

	if !p.expectPeek(token.FECHACHAVE) {
		return nil
	}

	return hash
}

// parseBlockStatement parses a block statement and returns the corresponding AST node.
func (p *Parser) parseBlockStatement() *ast.BlockStatement {
	block := &ast.BlockStatement{Token: p.currentToken}

	p.nextToken()

	for !p.currentTokenIs(token.FECHACHAVE) && !p.currentTokenIs(token.EOF) {
		stmt := p.parseStatement()
		if stmt != nil {
			block.Statements = append(block.Statements, stmt)
		}
		p.nextToken()
	}

	return block
}

// currentTokenIs checks if the current token's type is equal to the provided token type.
func (p *Parser) currentTokenIs(t token.TokenType) bool {
	return p.currentToken.Type == t
}

// parseStatement parses a statement and returns the corresponding AST node.
func (p *Parser) parseStatement() ast.Statement {
	switch p.currentToken.Type {
	case token.VAR:
		return p.parseVariableDeclaration()
	case token.IDENT:
		if p.peekTokenIs(token.ATRIBUICAO) {
			return p.parseAssignmentStatement()
		} else {
			return p.parseExpressionStatement()
		}
	case token.RETORNE:
		return p.parseReturnStatement()
	case token.SE:
		return p.parseIfExpression()
	case token.PARA:
		return p.parseForLoop()
	case token.ESCREVA:
		return p.parseOutputStatement()
	default:
		return p.parseExpressionStatement()
	}
}

// parseVariableDeclaration parses a variable declaration statement and returns the corresponding AST node.
func (p *Parser) parseVariableDeclaration() *ast.VariableDeclarationStatement {
	stmt := &ast.VariableDeclarationStatement{Token: p.currentToken}

	if !p.expectPeek(token.IDENT) {
		return nil
	}

	stmt.Name = &ast.Identifier{Token: p.currentToken, Value: p.currentToken.Literal}

	if !p.expectPeek(token.ATRIBUICAO) {
		return nil
	}

	p.nextToken()

	stmt.Value = p.parseExpression(LOWEST)

	if p.peekTokenIs(token.PONTOVIRGULA) {
		p.nextToken()
	}

	return stmt
}
func (p *Parser) peekTokenIs(t token.TokenType) bool {
	return p.peekToken.Type == t
}

// parseExpression parses an expression and returns the corresponding AST node.
func (p *Parser) parseExpression(precedence int) ast.Expression {
	left := p.parsePrefixExpression()
	if left == nil {
		return nil
	}

	for !p.peekTokenIs(token.PONTOVIRGULA) && precedence < p.peekPrecedence() {
		infix := p.parseInfixExpression(left)
		if infix == nil {
			return left
		}

		left = infix
	}

	return left
}

// parsePrefixExpression parses a prefix expression and returns the corresponding AST node.
func (p *Parser) parsePrefixExpression() ast.Expression {
	expression := &ast.PrefixExpression{
		Token:    p.currentToken,
		Operator: p.currentToken.Literal,
	}

	p.nextToken()

	expression.Right = p.parseExpression(PREFIX)

	return expression
}
func (p *Parser) parseInfixExpression(left ast.Expression) ast.Expression {
	infixToken := p.currentToken
	precedence := p.currentPrecedence()
	p.nextToken()
	right := p.parseExpression(precedence)
	return &ast.InfixExpression{
		Token:    infixToken,
		Left:     left,
		Operator: infixToken.Literal,
		Right:    right,
	}
}

// currentPrecedence returns the precedence of the current token.
func (p *Parser) currentPrecedence() int {
	if p, ok := precedences[p.currentToken.Type]; ok {
		return p
	}
	return LOWEST
}

// parseAssignmentStatement parses an assignment statement and returns the corresponding AST node.
func (p *Parser) parseAssignmentStatement() ast.Statement {
	statement := &ast.AssignmentStatement{Line: p.currentToken.Line, Char: p.currentToken.Char}

	// Parse the left-hand side of the assignment statement.
	statement.Value = p.parseExpression(LOWEST)

	// Verify that the left-hand side is a valid target for assignment.
	if _, ok := statement.Value.(*ast.VariableReference); !ok {
		p.error("Invalid target for assignment")
		return nil
	}

	// Verify that the assignment operator is present.
	if !p.expectPeek(token.ATRIBUICAO) {
		return nil
	}

	// Parse the right-hand side of the assignment statement.
	statement.Value = p.parseExpression(LOWEST)

	return statement
}

// parseExpressionStatement parses an expression statement and returns the corresponding AST node.
func (p *Parser) parseExpressionStatement() ast.Statement {
	statement := &ast.ExpressionStatement{Line: p.currentToken.Line, Char: p.currentToken.Char}

	statement.Expression = p.parseExpression(LOWEST)

	if p.peekTokenIs(token.PONTOVIRGULA) {
		p.nextToken()
	}

	return statement
}

// parseReturnStatement parses a return statement and returns the corresponding AST node.
func (p *Parser) parseReturnStatement() ast.Statement {
	stmt := &ast.ReturnStatement{Token: p.currentToken}

	p.nextToken()

	// parse the return value expression
	stmt.ReturnValue = p.parseExpression(LOWEST)

	// consume semicolon at the end of the statement
	if p.peekTokenIs(token.PONTOVIRGULA) {
		p.nextToken()
	}

	return stmt
}

func (p *Parser) parseIfExpression() ast.Statement {
	expression := &ast.IfExpression{Token: p.currentToken}

	if !p.expectPeek(token.ABREPAREN) {
		return nil
	}

	p.nextToken()

	expression.Condition = p.parseExpression(LOWEST)

	if !p.expectPeek(token.FECHAPAREN) {
		return nil
	}

	if !p.expectPeek(token.ABRECHAVE) {
		return nil
	}

	expression.Consequence = p.parseBlockStatement()

	if p.peekTokenIs(token.SENAO) {
		p.nextToken()

		if !p.expectPeek(token.ABRECHAVE) {
			return nil
		}

		expression.Alternative = p.parseBlockStatement()
	}

	return &ast.ExpressionStatement{Token: expression.Token, Expression: expression}
}

// parseForLoop parses a for loop and returns the corresponding AST node
func (p *Parser) parseForLoop() ast.Statement {
	// Parse the opening for loop token
	forToken := p.currentToken
	if !p.expectPeek(token.ABREPAREN) {
		return nil
	}

	// Parse the initializer
	var initializer ast.Statement
	if p.peekTokenIs(token.PONTOVIRGULA) {
		p.nextToken()
	} else if !p.peekTokenIs(token.PONTOVIRGULA) {
		initializer = p.parseStatement()
		if initializer == nil {
			return nil
		}
	}

	// Parse the conditional
	p.nextToken()
	condition := p.parseExpression(LOWEST)
	if condition == nil {
		return nil
	}
	if !p.expectPeek(token.PONTOVIRGULA) {
		return nil
	}

	// Parse the incrementer
	p.nextToken()
	var incrementer ast.Expression
	if !p.peekTokenIs(token.FECHAPAREN) {
		incrementer = p.parseExpression(LOWEST)
		if incrementer == nil {
			return nil
		}
	}
	if !p.expectPeek(token.FECHAPAREN) {
		return nil
	}

	// Parse the body
	body := p.parseBlockStatement()

	return &ast.ForLoopStatement{
		Token:     forToken,
		Init:      initializer,
		Condition: condition,
		Post:      incrementer,
		Body:      body,
	}
}

// error adds an error message to the parser's error list.
func (p *Parser) error(message string) {
	p.errors = append(p.errors, fmt.Sprintf("Syntax error: %s", message))
}

// ParseProgram parses a program and returns the corresponding AST node and an error, if any.
func (p *Parser) ParseProgram() (*ast.Program, error) {
	program := &ast.Program{}
	program.Statements = []ast.Statement{}

	for !p.currentTokenIs(token.EOF) {
		stmt := p.parseStatement()
		if stmt != nil {
			program.Statements = append(program.Statements, stmt)
		}
		p.nextToken()
	}

	return program, nil
}
