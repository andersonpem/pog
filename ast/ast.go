package ast

import (
	"bytes"
	"fmt"
	"pog/token"
	"strconv"
	"strings"
)

// Node is an interface implemented by all AST nodes.
type Node interface {
	TokenLiteral() string
	String() string
}

// Statement is an interface implemented by all statement nodes.
type Statement interface {
	Node
	statementNode()
}

// Expression is an interface implemented by all expression nodes.
type Expression interface {
	Node
	expressionNode()
}

// Program represents a Pog program.
type Program struct {
	Statements []Statement
}

// Identifier represents an identifier expression in the AST.
type Identifier struct {
	Token token.Token // the IDENT token
	Value string
}

type ForLoopStatement struct {
	Init      Statement
	Condition Expression
	Post      Expression
	Body      *BlockStatement
	Token     token.Token // the 'for' token
}

// ArrayLiteral represents an array literal.
type ArrayLiteral struct {
	Token    token.Token // the '[' token
	Elements []Expression
}

func (al *ArrayLiteral) expressionNode()      {}
func (al *ArrayLiteral) TokenLiteral() string { return al.Token.Literal }
func (al *ArrayLiteral) String() string {
	var out bytes.Buffer

	elements := []string{}
	for _, el := range al.Elements {
		elements = append(elements, el.String())
	}

	out.WriteString("[")
	out.WriteString(strings.Join(elements, ", "))
	out.WriteString("]")

	return out.String()
}

type StringLiteral struct {
	Token token.Token // The STRING token
	Value string
}

func (sl *StringLiteral) expressionNode() {}

// TokenLiteral returns the string literal's token literal.
func (sl *StringLiteral) TokenLiteral() string { return sl.Token.Literal }

func (sl *StringLiteral) String() string { return sl.Value }

// CallExpression represents a function call expression in the AST.
type CallExpression struct {
	Function  Expression
	Arguments []Expression
	Token     token.Token // The '(' token
}

type InputStatement struct {
	Prompt string      // prompt to display to the user
	Target *Identifier // variable to store the user's input in
	Token  token.Token // the 'input' token
}

func (is *InputStatement) statementNode()       {}
func (is *InputStatement) TokenLiteral() string { return is.Token.Literal }
func (is *InputStatement) String() string {
	var out bytes.Buffer

	out.WriteString(is.TokenLiteral() + " ")
	out.WriteString(is.Target.String() + " ")
	out.WriteString("= " + "input(" + strconv.Quote(is.Prompt) + ")" + ";")

	return out.String()
}

// OutputStatement represents an output statement.
type OutputStatement struct {
	Expression Expression
	Token      token.Token // the 'output' token
}

func (os *OutputStatement) statementNode()       {}
func (os *OutputStatement) TokenLiteral() string { return os.Token.Literal }
func (os *OutputStatement) String() string {
	var out bytes.Buffer

	out.WriteString(os.TokenLiteral() + " ")
	out.WriteString(os.Expression.String())
	out.WriteString(";")

	return out.String()
}

type HashLiteral struct {
	Pairs map[Expression]Expression
	Token token.Token // the '{' token
}

func (hl *HashLiteral) expressionNode() {}

// TokenLiteral returns the literal representation of the token
func (hl *HashLiteral) TokenLiteral() string { return hl.Token.Literal }

func (hl *HashLiteral) String() string {
	var out bytes.Buffer

	pairs := []string{}
	for key, value := range hl.Pairs {
		pairs = append(pairs, key.String()+":"+value.String())
	}

	out.WriteString("{")
	out.WriteString(strings.Join(pairs, ", "))
	out.WriteString("}")

	return out.String()
}

type VariableDeclarationStatement struct {
	Token token.Token // The 'let' token
	Name  *Identifier
	Value Expression
}

func (v *VariableDeclarationStatement) statementNode()       {}
func (v *VariableDeclarationStatement) TokenLiteral() string { return v.Token.Literal }
func (v *VariableDeclarationStatement) String() string {
	var out bytes.Buffer

	out.WriteString(v.TokenLiteral() + " ")
	out.WriteString(v.Name.String())
	out.WriteString(" = ")

	if v.Value != nil {
		out.WriteString(v.Value.String())
	}

	out.WriteString(";")

	return out.String()
}

// AssignmentStatement represents an assignment statement in the AST.
type AssignmentStatement struct {
	Token token.Token // the ASSIGN token
	Name  *Identifier
	Value Expression
	Line  int
	Char  int
}

func (as *AssignmentStatement) statementNode()       {}
func (as *AssignmentStatement) TokenLiteral() string { return as.Token.Literal }
func (as *AssignmentStatement) String() string {
	var out bytes.Buffer
	out.WriteString(as.Name.String())
	out.WriteString(" = ")
	out.WriteString(as.Value.String())
	out.WriteString(";")
	return out.String()
}

type VariableReference struct {
	Token token.Token // the identifier token
	Value string      // the value of the identifier
}

func (vr *VariableReference) expressionNode() {}

// TokenLiteral returns the literal representation of the token.
func (vr *VariableReference) TokenLiteral() string {
	return vr.Token.Literal
}

func (vr *VariableReference) String() string {
	return vr.Value
}

// TokenLiteral returns the literal representation of the token.
func (f ForLoopStatement) TokenLiteral() string {
	return f.Token.Literal
}

// String returns a string representation of the ForLoopStatement.
func (f ForLoopStatement) String() string {
	var out strings.Builder

	out.WriteString("for (")
	out.WriteString(f.Init.String())
	out.WriteString("; ")
	out.WriteString(f.Condition.String())
	out.WriteString("; ")
	out.WriteString(f.Post.String())
	out.WriteString(") ")
	out.WriteString(f.Body.String())

	return out.String()
}

func (f ForLoopStatement) statementNode() {}

func (i *Identifier) expressionNode() {}

// TokenLiteral returns the literal value of the token associated with the node.
func (i *Identifier) TokenLiteral() string {
	return i.Token.Literal
}

// String returns a string representation of the node.
func (i *Identifier) String() string {
	return i.Value
}

// AssignStatement represents an assignment statement in the AST.
type AssignStatement struct {
	Token token.Token // the ASSIGN token
	Name  *Identifier
	Value Expression
}

func (as *AssignStatement) statementNode() {}

// TokenLiteral returns the literal value of the token associated with the node.
func (as *AssignStatement) TokenLiteral() string {
	return as.Token.Literal
}

// String returns a string representation of the node.
func (as *AssignStatement) String() string {
	var out bytes.Buffer

	out.WriteString(as.Name.String())
	out.WriteString(" := ")
	out.WriteString(as.Value.String())
	out.WriteString(";")

	return out.String()
}

// ExpressionStatement represents an expression statement in the AST.
type ExpressionStatement struct {
	Token      token.Token // the first token of the expression
	Expression Expression
	Line       int
	Char       int
}

func (es *ExpressionStatement) statementNode() {}

// TokenLiteral returns the literal value of the token associated with the node.
func (es *ExpressionStatement) TokenLiteral() string {
	return es.Token.Literal
}

// String returns a string representation of the node.
func (es *ExpressionStatement) String() string {
	if es.Expression != nil {
		return es.Expression.String()
	}
	return ""
}

// BooleanLiteral represents a boolean literal expression in the AST.
type BooleanLiteral struct {
	Token token.Token
	Value bool
}

func (bl *BooleanLiteral) expressionNode() {}

// TokenLiteral returns the literal value of the token associated with the node.
func (bl *BooleanLiteral) TokenLiteral() string {
	return bl.Token.Literal
}

// String returns a string representation of the node.
func (bl *BooleanLiteral) String() string {
	return bl.Token.Literal
}

// IntegerLiteral represents an integer literal expression in the AST.
type IntegerLiteral struct {
	Token token.Token
	Value int64
}

func (il *IntegerLiteral) expressionNode() {}

// TokenLiteral returns the literal value of the token associated with the node.
func (il *IntegerLiteral) TokenLiteral() string {
	return il.Token.Literal
}

// String returns a string representation of the node.
func (il *IntegerLiteral) String() string {
	return il.Token.Literal
}

// PrefixExpression represents a prefix expression in the AST.
type PrefixExpression struct {
	Token    token.Token // The prefix token, e.g. !
	Operator string
	Right    Expression
}

func (pe *PrefixExpression) expressionNode() {}

// TokenLiteral returns the literal value of the token associated with the node.
func (pe *PrefixExpression) TokenLiteral() string {
	return pe.Token.Literal
}

// String returns a string representation of the node.
func (pe *PrefixExpression) String() string {
	var out bytes.Buffer

	out.WriteString("(")
	out.WriteString(pe.Operator)
	out.WriteString(pe.Right.String())
	out.WriteString(")")

	return out.String()
}

// InfixExpression represents an infix expression in the AST.
type InfixExpression struct {
	Token    token.Token // The operator token, e.g. +
	Left     Expression
	Operator string
	Right    Expression
}

func (ie *InfixExpression) expressionNode() {}

// TokenLiteral returns the literal value of the token associated with the node.
func (ie *InfixExpression) TokenLiteral() string {
	return ie.Token.Literal
}

// String returns a string representation of the node.
func (ie *InfixExpression) String() string {
	var out bytes.Buffer

	out.WriteString("(")
	out.WriteString(ie.Left.String())
	out.WriteString(" " + ie.Operator + " ")
	out.WriteString(ie.Right.String())
	out.WriteString(")")

	return out.String()
}

// ReturnStatement represents a return statement in the AST.
type ReturnStatement struct {
	Token       token.Token // the RETURN token
	ReturnValue Expression
}

func (rs *ReturnStatement) statementNode() {}

// TokenLiteral returns the literal value of the token associated with the node.
func (rs *ReturnStatement) TokenLiteral() string {
	return rs.Token.Literal
}

// String returns a string representation of the node.
func (rs *ReturnStatement) String() string {
	var out bytes.Buffer

	out.WriteString(rs.TokenLiteral() + " ")
	if rs.ReturnValue != nil {
		out.WriteString(rs.ReturnValue.String())
	}
	out.WriteString(";")

	return out.String()
}

// BlockStatement represents a block statement in the AST.
type BlockStatement struct {
	Token      token.Token // the { token
	Statements []Statement
}

func (bs *BlockStatement) statementNode() {}

// TokenLiteral returns the literal value of the token associated with the node.
func (bs *BlockStatement) TokenLiteral() string {
	return bs.Token.Literal
}

// String returns a string representation of the node.
func (bs *BlockStatement) String() string {
	var out bytes.Buffer

	for _, statement := range bs.Statements {
		out.WriteString(statement.String())
	}

	return out.String()
}

// IfExpression represents an if expression in the AST.
type IfExpression struct {
	Token       token.Token // The 'if' token
	Condition   Expression
	Consequence *BlockStatement
	Alternative *BlockStatement
}

func (ie *IfExpression) expressionNode() {}

// TokenLiteral returns the literal value of the token associated with the node.
func (ie *IfExpression) TokenLiteral() string {
	return ie.Token.Literal
}

// String returns a string representation of the node.
func (ie *IfExpression) String() string {
	var out bytes.Buffer

	out.WriteString("se ")
	out.WriteString(ie.Condition.String())
	out.WriteString(" então ")
	out.WriteString(ie.Consequence.String())

	if ie.Alternative != nil {
		out.WriteString(" senão ")
		out.WriteString(ie.Alternative.String())
	}

	return out.String()
}

// FunctionLiteral represents a function literal in the AST.
type FunctionLiteral struct {
	Token      token.Token // the FUNCTION token
	Parameters []*Identifier
	Body       *BlockStatement
}

func (fl *FunctionLiteral) expressionNode() {}

// TokenLiteral returns the literal value of the token associated with the node.
func (fl *FunctionLiteral) TokenLiteral() string {
	return fl.Token.Literal
}

func (p *Program) String() string {
	var out bytes.Buffer

	for _, s := range p.Statements {
		out.WriteString(s.String())
	}

	return out.String()
}

func (fl *FunctionLiteral) String() string {
	var params []string
	for _, p := range fl.Parameters {
		params = append(params, p.String())
	}
	return fmt.Sprintf("%s(%s) %s", fl.TokenLiteral(), strings.Join(params, ", "), fl.Body.String())
}

func (ce *CallExpression) String() string {
	var out bytes.Buffer

	args := []string{}
	for _, arg := range ce.Arguments {
		args = append(args, arg.String())
	}

	out.WriteString(ce.Function.String())
	out.WriteString("(")
	out.WriteString(strings.Join(args, ", "))
	out.WriteString(")")

	return out.String()
}

func (ce *CallExpression) TokenLiteral() string {
	return ce.Token.Literal
}

func (ce *CallExpression) expressionNode() {}
