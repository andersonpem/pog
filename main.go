package main

import (
	"fmt"
	"os"
	"pog/lexer"
	"pog/parser"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Fprintf(os.Stderr, "Usage: %s filename\n", os.Args[0])
		os.Exit(1)
	}

	filename := os.Args[1]
	input, err := os.ReadFile(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error reading file %s: %v\n", filename, err)
		os.Exit(1)
	}

	l := lexer.New(string(input))
	p := parser.New(l)

	program, err := p.ParseProgram()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error parsing program: %v\n", err)
		os.Exit(1)
	}

	fmt.Println(program.String())
}
